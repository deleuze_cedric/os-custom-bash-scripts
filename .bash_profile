
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

source $SCRIPT_DIR/.env

alias d='docker'
alias dc='docker compose'
alias d-log='d logs'
alias is_number_regex='^[0-9]+$'

function d-bash()
{
    d exec -it $1 bash
}

function dc-restart()
{
    dc down
    dc build
    dc up -d
}

function cd-project()
{
 cd $PROJECT_PATH
}

function web-pack-start()
{
   cd $WEB_PACK_PATH
   dc-restart
}


# Remplace la valeur $1 par la valeur $2 dans le fichier $3
function replace_in_file() {
 sudo sed -i -e "s/$1/$2/g" $3
}

function replace_db_url() {

 # Récupère le chemin du fichier sql
 echo "Chemin vers votre fichier sql (Rappel : Glisser déposer pour aller plus vite !) :"
 read sql
 
 sql=`echo $sql|tr -d \'`
 
 # Récupération de l'ancienne url
 echo "Ancien url (sans HTTP, Ex : code-agri.fr) :"
 read old_url
  
 # Récupération de la nouvelle url
 echo "Nouvel url (sans HTTP, Ex : dev.code-agri.fr) :"
 read new_url
  
 # Remplacer l'ancienne url par la nouvelle
 replace_in_file $old_url $new_url $sql
  
 # Remet en place les adresses mail
 replace_in_file "@$new_url" "@$old_url" $sql
  
 while true; do
  
 # Demande ce que souhaite faire l'utilisateur
 echo "HTTPS (1), HTTP (2), Ne rien faire (3) :"
 read http
  
 case $http in

 1)
 # Remplace les http par https
 replace_in_file "http:\/\/$new_url" "https:\/\/$new_url" $sql
 break
 ;;

 2)
 # Remplace les https par http
 replace_in_file "https:\/\/$new_url" "http:\/\/$new_url" $sql
 break
 ;;

 3)
 # Ne fait rien
 break
 ;;

 *)
 echo "Réponse non valide"
 ;;

 esac
 done
}

function install_docker(){
 sudo apt upgrade;
 sudo apt update;
 sudo apt-get install -y ca-certificates curl gnupg lsb-release;
 sudo mkdir -p /etc/apt/keyrings;
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg;
 echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null;
 sudo apt update;
 sudo apt install -y docker docker-ce docker-ce-cli containerd.io docker-compose-plugin;
 sudo groupadd docker;
 sudo usermod -aG docker $USER;
 newgrp docker;

}

function install_multipass(){
 sudo apt upgrade;
 sudo apt update;
 sudo apt install snap;
 sudo snap install multipass;
}

function init_multipass_docker_instance(){

 echo "Nom de l'instance :"
 read name

 echo "Mémoire vive (4Go par défaut) :"
 read memory

 if [ -z "$memory" ] || ![[ $memory =~ $is_number_regex ]];
 then
    memory=4
 fi

 memory="${memory}G"

 echo "Mémoire morte (30Go par défaut) :"
 read disk

 if [ -z "$disk" ] || ![[ $disk =~ $is_number_regex ]];
 then
    disk=30
 fi

 disk="${disk}G"

 echo "Nombre de coeur (4 par défaut) :"
 read cpus

 if [ -z "$cpus" ]|| ![[ $cpus =~ $is_number_regex ]];
 then
    cpus=4
 fi

 multipass launch --name $name --cpus $cpus --disk $disk --memory $memory --cloud-init $SCRIPT_DIR/cloud-init-docker-compose.yaml

}
